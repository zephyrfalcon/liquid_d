#

SRC = liquid.d tokenizer.d parser.d stringtools.d batchinterpreter.d \
      lqtypes.d environment.d listtools.d tools.d ast.d interactive.d \
      builtins.d lqtools.d special.d lists.d formals.d equivalence.d \
      environments.d lambdas.d numbers.d dictionaries.d files.d \
      metadata.d

TESTFILES = test_lqtypes.d test_batchinterpreter.d test_auto.d

.PHONY: test

liquid: $(SRC)
	gdc $(SRC) -o liquid

test: $(SRC) $(TESTFILES) tests/*.txt
	gdc $(SRC) $(TESTFILES) -o liquid-test -funittest -fversion=Test
	./liquid-test

clean:
	rm -f liquid liquid-test *.o
