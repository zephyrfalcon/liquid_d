Current status: Unmaintained. Mostly here for historical reasons.

This is an implementation of a Lisp dialect called Liquid. Code is written in
D; apparently the gdc compiler is necessary to build it, dmd doesn't seem to
work (at least not without major changes to the code).

