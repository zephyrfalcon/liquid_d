module builtins;

/* A few built-in functions... move these later. */

import environment;
import formals;
import lqtypes;


LqType s_type(LqType[] args, kwdict kwargs, Environment env) {
    assert(args.length == 1);
    return new LqSymbol(args[0].type_indicator());
}

LqType s_repr(LqType[] args, kwdict kwargs, Environment env) {
    assert(args.length == 1);
    return new LqString(args[0].lq_repr());
}

/* Return a unique identifier for the object, like Python's id() function.
   Currently based on address of the object. Not sure if this is safe (e.g.
   is the address guaranteed to remain the same during the program's lifetime?)
*/
LqType s_objid(LqType[] args, kwdict kwargs, Environment env) {
    assert(args.length == 1);
    ulong id = args[0].id();
    return new LqInteger(id);
}

/* alternatively, we could have a literal for this... */
LqType s_unspecified(LqType[] args, kwdict kwargs, Environment env) {
    return LQ_UNSPECIFIED();
}

bfun[string] get_builtins() {
    /* using an associative array literal here seems to have complications */
    bfun[string] z;
    z["type"]        = &s_type;
    z["repr"]        = &s_repr;
    z["objid"]       = &s_objid;
    z["unspecified"] = &s_unspecified;
    return z;
}

