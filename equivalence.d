module equivalence;

import environment;
import formals;
import lqtypes;

LqType s_equal_p(LqType[] args, kwdict kwargs, Environment env) {
    /* (equal? a b) */
    /* Return true if a and b have "the same value". Currently implemented as,
       their lq_repr()s are the same. */
    assert(args.length == 2);
    bool same = (args[0].lq_repr() == args[1].lq_repr());
    return same ? LQ_TRUE() : LQ_FALSE();
}

LqType s_is_p(LqType[] args, kwdict kwargs, Environment env) {
    /* (is? a b) */
    /* Return true if a and b refer to the same object (from D's POV). */
    assert(args.length == 2);
    bool same = (args[0] is args[1]);
    return same ? LQ_TRUE() : LQ_FALSE();
}

bfun[string] get_builtins() {
    bfun[string] z;
    z["equal?"] = &s_equal_p;
    z["is?"] = &s_is_p;
    return z;
}
