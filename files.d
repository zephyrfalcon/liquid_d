module files;

import std.stream;
import environment;
import formals;
import lqtools;
import lqtypes;

LqType s_file_open(LqType[] args, kwdict kwargs, Environment env) {
    /* (file-open <filename> <mode>) */
    /* I think mode should be a symbol... 'read, 'write, 'append ...? */
    /* Or we could use keywords :read and :write; if both are given, we
       append; if neither are given, we assume :read. */
    check_arglength(args, 1, 1, "file-open");
    check_arg(args[0], 0, "string", "file-open");
    bool kw_read = (get_keyword(kwargs, ":read") !is LQ_FALSE);
    bool kw_write = (get_keyword(kwargs, ":write") !is LQ_FALSE);
    
    FileMode mode;
    if (kw_read && kw_write)
        mode = FileMode.Append;
    else if (kw_read)
        mode = FileMode.In;
    else if (kw_write)
        mode = FileMode.Out;
    else
        mode = FileMode.In;
        
    string filename = (cast(LqString)(args[0])).lq_str();
    return new LqFile(filename, mode);
}

LqType s_file_close(LqType[] args, kwdict kwargs, Environment env) {
    /* (file-close f) */
    check_arglength(args, 1, 1, "file-close");
    check_arg(args[0], 0, "file", "file-close");
    auto lqf = cast(LqFile)(args[0]);
    File f = lqf.file();
    f.close();
    return LQ_UNSPECIFIED();
}

bfun[string] get_builtins() {
    /* using an associative array literal here seems to have complications */
    bfun[string] z;
    z["file-open"]  = &s_file_open;
    z["file-close"] = &s_file_close;
    return z;
}
