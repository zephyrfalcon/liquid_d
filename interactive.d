module interactive;

import std.stdio;
import std.string;
import batchinterpreter;
import lqtypes;

class Interactive {
    
    private BatchInterpreter _interpreter;
    public string prompt = ">";
    
    this() {
        _interpreter = new BatchInterpreter();
        _interpreter.show_stack = true;
        _interpreter.show_elapsed = true;
    }
    
    void process_line(string line) {
        LqType result = this._interpreter.eval(line);
        if (result !is null)
            writefln("=> %s", result.lq_repr());
    }
    
    void run() {
        while (1) {
            string buf = "";
            writef(this.prompt ~ " ");
            buf = readln();
            if (buf !is null && buf != "") {
                if (std.string.strip(buf) == ",t") {
                    writefln("Last evaluation took %d", this._interpreter.elapsed);
                    continue;
                }
                
                try {
                    process_line(buf);
                }
                catch (Exception e) {
                    writefln("Error: %s", e.msg);
                }
            if (buf is null) break;
            }
        }
    }
}
