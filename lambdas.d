module lambdas;

import environment;
import formals;
import lqtools;
import lqtypes;

/* TODO: lambda-signature 
         make-lambda
*/

LqType s_lambda_body(LqType[] args, kwdict kwargs, Environment env) {
    /* (lambda-body <function>) => list of exprs */
    check_arglength(args, 1, 1, "lambda-body");
    check_arg(args[0], 0, "userdef-function", "lambda-body");
    auto udf = cast(LqUserDefinedFunction)(args[0]);
    LqList lbody = LqList.from_list(udf.fbody());
    return lbody;
}

LqType s_lambda_environment(LqType[] args, kwdict kwargs, Environment env) {
    /* (lambda-environment <function>) => environment */
    check_arglength(args, 1, 1, "lambda-environment");
    check_arg(args[0], 0, "userdef-function", "lambda-environment");
    auto udf = cast(LqUserDefinedFunction)(args[0]);
    return new LqEnvironment(udf.env());
}


bfun[string] get_builtins() {
    /* using an associative array literal here seems to have complications */
    bfun[string] z;
    z["lambda-body"]        = &s_lambda_body;
    z["lambda-environment"] = &s_lambda_environment;
    return z;
}

