/* liquid.d */

import interactive;

int main(string[] args) {
    
    /* if we're in test mode, just exit. */
    version (Test) { return 0; }
    
    Interactive i = new Interactive();
    i.run();
    return 0;
}
