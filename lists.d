module lists;

import environment;
import formals;
import lqtools;
import lqtypes;

LqType s_pair(LqType[] args, kwdict kwargs, Environment env) {
    assert(args.length == 2);
    //check_arg(args[1], 1, "pair", "pair");
    assert(args[1].is_list(), "pair: second item must be a list");
    return new LqPair(args[0], as_list(args[1]));
}

LqType s_head(LqType[] args, kwdict kwargs, Environment env) {
    assert(args.length == 1 && args[0].is_list());
    check_arg(args[0], 0, "pair", "pair");
    return (cast(LqPair)args[0]).head();
}

LqType s_tail(LqType[] args, kwdict kwargs, Environment env) {
    assert(args.length == 1 && args[0].is_list());
    return (cast(LqPair)args[0]).tail();
}

bfun[string] get_builtins() {
    bfun[string] z;
    z["pair"] = &s_pair;
    z["head"] = &s_head;
    z["tail"] = &s_tail;
    return z;
}

