module listtools;

import std.stdio;
import stringtools;

string[] unique(string[] items) {
    string[] newlist = [];
    foreach (string s; items) {
        if (!stringtools.string_in_list(s, newlist))
            newlist ~= s;
    }
    return newlist;
}

string[] remove(string[] items, string removethis) {
    string[] keep = [];
    foreach (string s; items) {
        if (s != removethis)
            keep ~= s;
    }
    return keep;
}

unittest {
    writefln("unittest: listtools");
    
    string[] stuff = ["a", "b", "c", "d"];
    assert(remove(stuff, "c") == ["a", "b", "d"]);
    assert(remove(stuff, "e") == stuff);
}
