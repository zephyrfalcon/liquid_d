module metadata;

import std.array;
import environment;
import formals;
import lqtools;
import lqtypes;

LqType s_metadata_set(LqType[] args, kwdict kwargs, Environment env) {
    /* (metadata-set! obj name value) */
    check_arglength(args, 3, 3, "metadata-set!");
    check_arg(args[1], 1, "symbol", "metadata-set!");
    
    string s = (cast(LqSymbol)(args[1])).toString();
    args[0].set_meta(s, args[2]);
    return LQ_UNSPECIFIED();
}

LqType s_metadata(LqType[] args, kwdict kwargs, Environment env) {
    /* (metadata obj) */
    check_arglength(args, 1, 1, "metadata");
    
    LqType[] items = [];
    foreach(string name; args[0].meta.keys.sort) {
        LqType value = args[0].get_meta(name);
        LqType sname = new LqSymbol(name);
        LqType[] pair = [sname, value];
        LqList lpair = LqList.from_list(pair);
        items ~= lpair;
    }
    
    return LqList.from_list(items);
}

LqType s_metadata_get(LqType[] args, kwdict kwargs, Environment env) {
    /* (metadata-get obj name) => value or #f */
    check_arglength(args, 2, 2, "metadata-get");
    check_arg(args[1], 1, "symbol", "metadata-get");
    
    string s = (cast(LqSymbol)(args[1])).toString();
    LqType result;
    
    try {
        result = args[0].get_meta(s);
    }
    catch (ArrayBoundsError e) {
        result = LQ_FALSE();
    }
    
    return result;
}

bfun[string] get_builtins() {
    /* using an associative array literal here seems to have complications */
    bfun[string] z;
    z["metadata-set!"] = &s_metadata_set;
    z["metadata-get"]  = &s_metadata_get;
    z["metadata"]      = &s_metadata;
    return z;
}
