#

proposed file "API":

(file-open filename :read :write) => file-object

(file-close <file-object>)

(file-read [n]) => string
(file-write data)

(file-modes f) => list of 'read and/or 'write
(file-name f) => filename

