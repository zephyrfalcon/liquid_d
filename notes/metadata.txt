# metadata

Each object (instance of LqType) has a dictionary with metadata. By default,
this is empty. Values can set by mapping a name (symbol) to any LqType
instance.

Functions:

(metadata x) ;; returns an alist

(metadata-get x name)

(metadata-set! x name value)

(metadata-delete! x name)

(metadata-keys x) ;; derived

(with-metadata
  ..definitions..
  expr)


Define/lambdas:

...


(lambda (x y z)
  (:foo 1)
  (:bar (f "this" "that"))
  (+ x y z))

==>

  (with-metadata
    ((foo 1)
     (bar (f "this" "that")))
    (lambda (x y z)
      (+ x y z)))

Sooo, if we see expressions starting with a keyword inside a lambda/define
body, then we need to transform the expression to WITH-METADATA.

OR, we can store inline metadata unevaluated... but then how do you get the
value? Wrap a lambda around it?
OR, we only allow simple expressions like string literals and numbers... no
compound expressions.
That would work for docstrings, but I am not sure if that kind of
inflexibility is a good idea.

OR, we don't allow it at all, and use WITH-METADATA or METADATA-SET! for all
cases.

OR, when dealing with the lambda, we evaluate these expressions, stick the
evaluated form back, and store them in LqUserDefinedFunction. Hmm...

We might be better off transforming the lambda/define...

Write a function that does this, then have the batchinterpreter use it.

