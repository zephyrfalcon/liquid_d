module numbers;

import environment;
import formals;
import lqtypes;
import lqtools;

/* TODO: should support ints, floats, etc, and return the appropriate result */
LqType s_plus(LqType[] args, kwdict kwargs, Environment env) {
    long result = 0; /* for now, only integers */
    foreach (LqType q; args) {
        assert(q.type_indicator() == "integer");
        result += (cast(LqInteger)q).value();
    }
    return new LqInteger(result);
}

LqType s_minus(LqType[] args, kwdict kwargs, Environment env) {
    /* (- n <numbers>*) */
    check_arglength(args, 1, -1, "-");
    if (args.length == 1) {
        assert(args[0].type_indicator() == "integer");
        long i = (cast(LqInteger)(args[0])).value();
        return new LqInteger(-i);
    }
    else {
        long start = (cast(LqInteger)args[0]).value();
        foreach(LqType q; args[1..args.length]) {
            assert(q.type_indicator() == "integer");
            long i = (cast(LqInteger)q).value();
            start = start - i;
        }
        return new LqInteger(start);
    }
}

/*** comparison operators ***/
/* for now, these only accept integers, and we use < and = to build
   the others. */

LqType s_lt(LqType[] args, kwdict kwargs, Environment env) {
    /* (< a b)
       For now, only accept two arguments. */
    check_arglength(args, 2, 2, "<");
    check_arg(args[0], 0, ["integer", "float"], "<");
    check_arg(args[1], 1, ["integer", "float"], "<");
    
    auto a1 = cast(LqInteger)(args[0]);
    auto a2 = cast(LqInteger)(args[1]);
    bool lt = a1.value() < a2.value();
    return lt ? LQ_TRUE() : LQ_FALSE();
}

LqType s_eq(LqType[] args, kwdict kwargs, Environment env) {
    /* (< a b)
       For now, only accept two arguments. */
    check_arglength(args, 2, 2, "=");
    check_arg(args[0], 0, ["integer", "float"], "=");
    check_arg(args[1], 1, ["integer", "float"], "=");
    
    auto a1 = cast(LqInteger)(args[0]);
    auto a2 = cast(LqInteger)(args[1]);
    bool lt = a1.value() == a2.value();
    return lt ? LQ_TRUE() : LQ_FALSE();
}

LqType s_le(LqType[] args, kwdict kwargs, Environment env) {
    /* (<= a b)
       For now, only accept two arguments. */
    check_arglength(args, 2, 2, "<=");
    check_arg(args[0], 0, ["integer", "float"], "<=");
    check_arg(args[1], 1, ["integer", "float"], "<=");
    
    auto a1 = cast(LqInteger)(args[0]);
    auto a2 = cast(LqInteger)(args[1]);
    bool lt = a1.value() <= a2.value();
    return lt ? LQ_TRUE() : LQ_FALSE();
}


bfun[string] get_builtins() {
    /* using an associative array literal here seems to have complications */
    bfun[string] z;
    z["+"]    = &s_plus;
    z["-"]    = &s_minus;
    z["<"]    = &s_lt;
    z["="]    = &s_eq;
    z["<="]   = &s_le;
    return z;
}

