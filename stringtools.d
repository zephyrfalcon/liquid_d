module stringtools;

import std.stdio;
import std.string;

bool string_in_list(string s, string[] choices) {
    foreach(string t; choices) {
        if (s == t) return true;
    }
    return false;
}

bool starts_with(string s, string sub) {
    if (sub.length > s.length) return false;
    return s[0..sub.length] == sub;
}

bool ends_with(string s, string sub) {
    if (sub.length > s.length) return false;
    return s[s.length-sub.length..s.length] == sub;
}

/* XXX this escape/unescape stuff needs to be done better... maybe we also
   want to support \xnn syntax, Unicode, etc. */

const string[2][] ESCAPE_CHARS = [
    ["\\", "\\"],
    ["n", "\n"],
    ["t", "\t"],
    ["\"", "\""],
];

string escape(string s) {
    foreach(string[2] esc; ESCAPE_CHARS) {
        s = std.string.replace(s, esc[1], "\\" ~ esc[0]);
    }
    return s;
}

string unescape(string s) {
    foreach(string[2] esc; ESCAPE_CHARS) {
        s = std.string.replace(s, "\\" ~ esc[0], esc[1]);
    }
    return s;
}

unittest {
    writefln("unittest: stringtools");
    
    string[] names = ["Guido", "Larry", "Matz"];
    assert(string_in_list("Guido", names) == true);
    assert(string_in_list("guido", names) == false);
    assert(string_in_list("Hans", names) == false);
    
    assert(starts_with("Hans", "Ha") == true);
    assert(starts_with("Hans", "Ho") == false);
    assert(starts_with("Hans", "Hansaplast") == false);
    
    assert(ends_with("Hans", "ns") == true);
    assert(ends_with("Hans", "Hans") == true);
    assert(ends_with("Hans", "Pans") == false);
    
}
