module test_batchinterpreter;

import std.string;
import std.stdio;
import lqtypes;
import batchinterpreter;
import lqtools;
import special;

unittest {
    writefln("unittest: batchinterpreter");
    
    void test_is_special_form() {
        LqType expr;
        expr = parser.parse(["(", "quote", "x", ")"])[0];
        assert(is_special_form(expr));
        expr = parser.parse(["(", "f", "x", ")"])[0];
        assert(!is_special_form(expr));
    }
    
    void test_sf_must_evaluate() {
        LqType expr;
        LqType[] processed = [];
        expr = parser.parse(["(", "quote", "x", ")"])[0];
        assert(sf_must_evaluate(processed, 2) == false);
        processed = [as_pair(expr).head()];
        assert(sf_must_evaluate(processed, 2) == false);
    }
    
    void assert_eval(string expr, string expected_result) {
        auto BatchInterpreter bi = new BatchInterpreter();
        bi.show_stack = false;
        LqType result = bi.eval(expr);
        assert(result.lq_repr() == expected_result,
          format("%s should result in %s; got %s instead",
            expr, expected_result, result.lq_repr()));
    }
    
    void test_eval() {
        /* simple tests to make sure we get the right result and things
           don't hang, etc. more sophisticated tests are elsewhere. */
        string[][] data = [
            ["42", "42"],
            ["(quote x)", "x"],
            ["(lambda (x) x)", "#<lambda (x)>"],
            ["(if #t 2 3)", "2"],
            ["(if #f 2 3)", "3"],
            ["(begin 1 2 3)", "3"],
            ["(define (f x) (+ x 1)) (f 3)", "4"],
            ["(define x 100) (define (f y) (+ x y)) (f 3)", "103"],
            ["((lambda (z) 1 z) 4)", "4"],
            ["(apply + '(1 2 3))", "6"],
        ];
        
        foreach(string[] ss; data) {
            string expr = ss[0];
            string expected = ss[1];
            assert_eval(expr, expected);
        }
    }
    
    /* run tests... */
    test_is_special_form();
    test_sf_must_evaluate();
    test_eval();
}
