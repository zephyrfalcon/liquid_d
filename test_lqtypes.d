module test_lqtypes;

import std.stdio;
import lqtypes;
import parser;

unittest {
    writefln("unittest: lqtypes");
    
    void test_lq_repr() {
        assert((new LqSymbol("quote")).lq_repr(), "quote");
    }
    
    /* run tests... */
    test_lq_repr();
}
