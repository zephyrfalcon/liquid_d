;; conditionals.txt
;; Test Liquid-defined conditionals, like AND, OR, COND, CASE, etc.

;; --- and ---

(and #t #t 3 "" 0)
=> 0
(define xyzzy 4)
(and #f (set! xyzzy 5) #t 1900)
=> #f
xyzzy ;; should be unaffected
=> 4

;; --- or ---

(or #f (= 1 2) 19 #f)
=> 19

;; --- not ---

(not 33)
=> #f
(not #f)
=> #t
(not (= 3 3))
=> #f

;; --- when ---

(define a 1)
(when #t
  (set! a 2)
  (set! a 3))
a
=> 3
(unless #t
  (set! a 4))
a
=> 3

