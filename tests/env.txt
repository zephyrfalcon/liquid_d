;; env.txt
;; Test environments.

#!clear
(define e (current-environment))
(type e)
=> environment
(environment-names e)
=> (e)
(define f 42)
(environment-names e)
=> (e f)
(define a 'hello)
(environment-names e)
=> (a e f)

(second (environment-lookup e 'f))
=> 42
(environment-lookup e 'z)
=> #f

(environment-delete! e 'f)
(environment-names e)
=> (a e)

(environment-bind! e 'quux 101)
(environment-names e)
=> (a e quux)
quux
=> 101

(define e1 (make-environment))
(define e2 (make-environment e))
(environment-names e2)
=> ()
(environment-bind! e2 'baz 22)
(environment-names e2)
=> (baz)

;; XXX environment-all-names needs more testing than this
(environment-all-names e1)
=> ()

#!clear
(define x 0)
(define e1 (make-environment (current-environment)))
(environment-set! e1 'x 1)
x
=> 1

