;; eval.txt

;; --- plain eval ---

(eval 3)
=> 3

(define x 4)
(eval x)
=> 4

(eval '(+ x 1))
=> 5

;; --- eval-in ---

(define a 10)
(define e (make-environment (current-environment)))
(environment-bind! e 'a 100)
(eval-in '(+ a 1) e)
=> 101

(define a 10)
(define (f *rest...)
  ;; evaluates the first item, ignores the rest.
  (let ((rest (delayed-expr *rest...)))
    (if (empty? rest)
        #f
        (eval-in (head rest) *rest...))))
(f (+ a 1) (+ b 3) (* c 4) bogus)
=> 11

