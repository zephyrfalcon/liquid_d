;; if.txt

(if #t 2 3)
=> 2
(if #f 2 3)
=> 3

(if #t (pair 1 ()) (pair 2 ()))
=> (1)

