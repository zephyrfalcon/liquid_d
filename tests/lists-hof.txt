;; lists-hof.txt
;; higher-order functions

(map (lambda (x) (+ x 1))
     '(1 2 3))
=> (2 3 4)

(define (twice x) (+ x x))
(map twice '(1 2 3))
=> (2 4 6)

(map first '((a 1) (b 2) (c 3)))
=> (a b c)

(filter (lambda (x) (equal? x 'a))
        '(a b c a b c))
=> (a a)

#!clear
(define a 0)
(for-each (lambda (x) (set! a (+ a x))) 
          '(1 2 3))
a
=> 6

(fold-left + '(1 2 3 4) 0)
=> 10
(fold-right + '(1 2 3 4) 0)
=> 10
;; XXX need more tests

