;; metadata.txt

(define (f x) x)
(metadata-set! f 'doc "just a function")
(f 3)
(metadata-get f 'doc)
=> "just a function"

(define (f x) x)
(metadata-set! f 'doc "just a function")
(metadata-set! f 'subtype 'very-special-function)
(metadata-keys f)
=> (doc subtype)

(define (f x) x)
(metadata-set! f 'x 1)
(metadata-set! f 'y 2)
(metadata f)
=> ((x 1) (y 2))

(define g
  (with-metadata
    ((x 3)
     (y (+ 10 20)))
    (lambda (x) x)))
(metadata g)
=> ((x 3) (y 30))

; LATER:
;(define (h x y)
;  (:doc "some docstring")
;  (:signature (list number? number?))
;  (+ x y))
;(metadata-keys h)
;=> (doc signature)

