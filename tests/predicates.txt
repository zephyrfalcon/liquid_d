;; predicates.txt

(string? "xyz")
=> #t
(string? "")
=> #t
(string? 'xyz)
=> #f

(empty? '(a b c))
=> #f
(empty? 42)
=> #f
(empty? ())
=> #t
(empty? '(33))
=> #f

(pair? ())
=> #f
(pair? 42)
=> #f
(pair? '(x))
=> #t
(pair? '(x y z))
=> #t

