;; type.txt

(type "hello")
=> string

(type 42)
=> integer

(type 'foo)
=> symbol

(type +)
=> builtin-function

(type (lambda (x) x))
=> userdef-function

(type ':klixklaaxtl)
=> keyword

(type ())
=> empty-list

(type '(1 2 3))
=> pair

(type (unspecified))
=> unspecified

