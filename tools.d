module tools.d;
/* Some of these are probably already defined in the standard library; will
   figure that out (and replace them) later. */

long max(long a, long b) {
    return (a > b) ? a : b;
}

long min(long a, long b) {
    return (a < b) ? a : b;
}

